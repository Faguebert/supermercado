package br.com.supermercado;

import java.util.ArrayList;
import java.util.Scanner;

import br.com.supermercado.models.Cliente;
import br.com.supermercado.models.FormaPagamento;
import br.com.supermercado.models.Produto;
import br.com.supermercado.models.Venda;

public class Visualizacao {
	
	static int escolha;
	static ArrayList<Cliente> clientes = new ArrayList<Cliente>();
	static ArrayList<Produto> produtos = new ArrayList<Produto>();
	static ArrayList<FormaPagamento> formasPagamento = new ArrayList<FormaPagamento>();
	static ArrayList<Venda> vendas = new ArrayList<Venda>();

	public static void main(String[] args) {
		
		menu();
		while (escolha != 0) {
			if(escolha == 1)
				cadastrarCliente();
			else if(escolha == 2)
				cadastrarProduto();
			else if(escolha == 3)
				cadastrarFormaPagamento();
			else if(escolha == 4)
				cadastrarVenda();
			else if(escolha == 5)
				listarVenda();
			else if(escolha == 6)
				listarVendaPorFormaPagamento();
			else
				throw new IllegalArgumentException("op��o inv�lida! \n");
		}
		
		System.out.println("Saindo do supermercado!");
		System.out.println("Volte sempre...");
	}
	
	

	private static void listarVendaPorFormaPagamento() {
		System.out.println("Digite a forma de pagamento: ");
		String formaPagamentoProcurada = getString();
		
		
		System.out.println("=============================================");
		System.out.println("LISTA DE VENDAS POR " + formaPagamentoProcurada.toUpperCase() + " ");
		System.out.println("=============================================");
		
		for (Venda venda : vendas) {
			if(venda.getFormaPagamento().getNome().equals(formaPagamentoProcurada)) {
				System.out.println("CPF do cliente: " + venda.getCliente().getCpf());
				System.out.println("Nome do cliente: " + venda.getCliente().getNome());
				System.out.println("Nome do produto: " + venda.getProduto().getNomeProduto());
				System.out.println("Quantidade do produto: " + venda.getProduto().getQuantidade());
				System.out.println("Nome da forma de pagamento: " + venda.getFormaPagamento().getNome());
				System.out.println("=============================================");
			}
		}
		menu();
	}



	private static void listarVenda() {
		System.out.println("\n==================================");	
		System.out.println("         LISTA DE VENDAS          ");
		System.out.println("==================================");
		for (Venda venda : vendas) {
			System.out.println("CPF do Cliente: " + venda.getCliente().getCpf());
			System.out.println("Nome do Cliente: " + venda.getCliente().getNome());
			System.out.println("Nome do Produto: " + venda.getProduto().getNomeProduto());
			System.out.println("Quantidade do Produto: " + venda.getProduto().getQuantidade());
			System.out.println("Nome Forma de Pagamento: " + venda.getFormaPagamento().getNome());
			System.out.println("================================");
		}
		menu();
	}

	private static void cadastrarVenda() {
		Venda venda = new Venda();
		
		System.out.println("================================");
		System.out.println("       CADASTRO DE VENDAS       ");
		System.out.println("================================");
		System.out.println("Abaixo os clientes cadastrados: ");
		
		listarCliente();
		
		System.out.println("Digite o CPF do cliente que deseja realizar a compra:");
		String cpfCliente = getString();
		
		venda.setCliente(findClienteByCpf(cpfCliente));
		
		System.out.println("\nAbaixo os produtos cadastrados: ");
		
		listarProduto();
		
		System.out.println("Digite o nome do produto que deseja realizar a compra:");
		String nomeProduto = getString();

		venda.setProduto(findProdutoByNome(nomeProduto));
		
		System.out.println("\nAbaixo as formas de pagamento cadastradas: ");
		
		listarFormaDePagamento();
		
		System.out.println("Digite o nome da forma de pagamento que deseja realizar a compra:");
		String nomeformaPagamento = getString();
		
		venda.setFormaPagamento(findFormaPagamentoByNome(nomeformaPagamento));
		
		vendas.add(venda);
		
		System.out.println("\nVenda cadastrada com sucesso!");
		menu();
	}

	private static FormaPagamento findFormaPagamentoByNome(String nomeformaPagamento) {
		FormaPagamento formaPagamentoSelecionado = new FormaPagamento();
		
		for (FormaPagamento formaPagamento : formasPagamento) {
			if(formaPagamento.getNome().equals(nomeformaPagamento)) {
				formaPagamentoSelecionado.setNome(formaPagamento.getNome());
				return formaPagamentoSelecionado;
			}
		}
		return null;
	}

	private static Produto findProdutoByNome(String nomeProduto) {
		Produto produtoSelecionado = new Produto();
		
		for (Produto produto : produtos) {
			if(produto.getNomeProduto().equals(nomeProduto)) {
				produtoSelecionado.setNomeProduto(produto.getNomeProduto());
				produtoSelecionado.setQuantidade(produto.getQuantidade());
				return produtoSelecionado;
			}
		}
		return null;
	}

	private static Cliente findClienteByCpf(String cpf) {
		Cliente clienteSelecionado = new Cliente();
		
		for (Cliente cliente : clientes) {
			if(cliente.getCpf().equals(cpf)) {
				clienteSelecionado.setCpf(cliente.getCpf());
				clienteSelecionado.setNome(cliente.getNome());
				return clienteSelecionado;
			}
		}
		return null;
	}

	private static void listarFormaDePagamento() {
			System.out.println("\n  LISTA DE FORMAS DE PAGAMENTO  ");
		for (FormaPagamento formaPagamento : formasPagamento) {
			System.out.println("================================");
			System.out.println("Nome: " + formaPagamento.getNome());
			System.out.println("================================");
		}
	}

	private static void cadastrarFormaPagamento() {
		FormaPagamento formaPagamento = new FormaPagamento();
		
		System.out.println("================================");
		System.out.println(" CADASTRO DE FORMA DE PAGAMENTO ");
		System.out.println("================================");
		System.out.println("Digite o nome da forma de pagamento:");
		String nomePagamento = getString();
		
		formaPagamento.setNome(nomePagamento);
		
		formasPagamento.add(formaPagamento);
		
		System.out.println("Forma de pagamento adicionada com sucesso! \n");
		menu();
	}
	
	private static void listarProduto() {
			System.out.println("\n       LISTA DE PRODUTOS        ");
		for (Produto produto : produtos) {
			System.out.println("================================");
			System.out.println("Nome: " + produto.getNomeProduto());
			System.out.println("Quantidade: " + produto.getQuantidade());
			System.out.println("================================");
		}
	}

	private static void cadastrarProduto() {
		Produto produto = new Produto();
		
		System.out.println("================================");
		System.out.println("      CADASTRO DE PRODUTO       ");
		System.out.println("================================");
		System.out.println("Digite o nome do produto:");
		String nomeProduto = getString();
		System.out.println("Digite a quantidade do produto:");
		int quantidade = getInt();
		
		produto.setNomeProduto(nomeProduto);
		produto.setQuantidade(quantidade);
		
		produtos.add(produto);
		
		System.out.println("Produto adicionado com sucesso! \n");
		menu();
	}
	
	private static void listarCliente() {
			System.out.println("\n       LISTA DE CLIENTES        ");
		for (Cliente cliente : clientes) {
			System.out.println("================================");
			System.out.println("CPF: " + cliente.getCpf());
			System.out.println("Nome: " + cliente.getNome());
			System.out.println("================================");
		}
	}

	private static void cadastrarCliente() {
		Cliente cliente = new Cliente();
		
		System.out.println("================================");
		System.out.println("      CADASTRO DE CLIENTE       ");
		System.out.println("================================");
		System.out.println("Digite o CPF do cliente:");
		String cpf = getString();
		System.out.println("Digite o nome do cliente:");
		String nome = getString();
		
		cliente.setCpf(cpf);
		cliente.setNome(nome);
		
		clientes.add(cliente);
		
		System.out.println("\nCliente adicionado com sucesso! \n");
		menu();
	}

	private static String getString() {
		Scanner scanner = new Scanner(System.in);
		return scanner.nextLine();
	}

	private static Integer getInt() {
		Scanner scanner = new Scanner(System.in);
		return scanner.nextInt();
	}

	private static void menu() {
		System.out.println("================================");
		System.out.println("      SUPERMERCADO DO BOB       ");
		System.out.println("================================");
		System.out.println("1. Cadastrar cliente;");
		System.out.println("2. Cadastrar produto;");
		System.out.println("3. Cadastrar forma de pagamento;");
		System.out.println("4. Cadastrar venda;");
		System.out.println("5. Listar venda;");
		System.out.println("6. Listar vendas por forma de pagamento;");
		System.out.println("0. Sair.");
		System.out.println("\nO que deseja fazer?");
		escolha = getInt();
	}
}
